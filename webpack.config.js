const path = require('path');
const webpack = require('webpack');
const sourcePath = path.join(__dirname, 'assets/js');

const precss = require('precss');
const autoprefixer = require('autoprefixer');
const atImport = require('postcss-import');
const colorFunction = require('postcss-color-function');
const calc = require('postcss-calc');

module.exports = {
  context: sourcePath,
  entry:  './entry.js',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/build/',
  },
  resolve: {
    modulesDirectories: ['node_modules', 'assets/js'],
    extensions: ['', '.js'],
    alias: {
      fs: path.join(__dirname, 'assets/js/mocks/fs'),
      css: path.join(__dirname, 'assets/css'),
    },
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loaders: ['babel'],
    }, {
      test: /assets.css.modules.*\.css$/,
      loaders: [
        'style-loader',
        'css-loader?modules&importLoaders=1',
        'postcss-loader',
      ],
    }, {
      test: [
        /node_modules.*\.css$/,
        /assets.css.plain.*\.css$/,
      ],
      loaders: ['style', 'css'],
    }, {
      test: /\.(png|ttf|eot|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader',
    }, {
      test: /\.(png|gif)/,
      loader: 'file-loader',
    }],
  },
  postcss() {
    return [
      atImport({
        path: ['assets/css'],
      }),
      precss(),
      calc(),
      colorFunction(),
      autoprefixer({ browsers: ['last 2 versions'] }),
    ];
  },
};
