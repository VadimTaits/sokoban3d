import fs from 'fs';
import path from 'path';

import express from 'express';
const app = express();

import favicon from 'serve-favicon';

app.use(favicon(path.join(__dirname, '/static/favicon.png')));

app.set('views', './pug');
app.set('view engine', 'pug');

// TO DO: nginx proxy
app.use('/build', express.static(path.join(__dirname, 'build')));
app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/api/levels/', (req, res) => {
  const levelNumber = req.query.prev ?
    parseInt(req.query.prev) + 1 :
    0;

  fs.readFile(`./levels/level${ levelNumber }`, 'utf-8', (err, data) => {
    if (err) {
      res.end(JSON.stringify({
        found: false,
        level: null,
      }));

      return;
    }

    res.end(JSON.stringify({
      found: true,
      level: data,
    }));
  });
});

app.get('/', (req, res) => {
  res.render('index');
});

export default app;
