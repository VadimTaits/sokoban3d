export default {
  readFileSync: () => {
    throw new Error('Reading from file is not allowed in browser');
  },
};
