export const ROTATE_LEFT = Symbol('rotate left');
export const ROTATE_RIGHT = Symbol('rotate right');

export const STEP_FORWARD = Symbol('step forward');
export const STEP_BACKWARD = Symbol('step backward');
export const STEP_LEFT = Symbol('step left');
export const STEP_RIGHT = Symbol('step right');