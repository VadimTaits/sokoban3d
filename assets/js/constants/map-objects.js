export const EMPTY = Symbol('empty');
export const WALL = Symbol('wall');
export const BOX = Symbol('box');
export const END_POINT = Symbol('end point');
export const BOX_ON_END_POINT = Symbol('box on end point');

