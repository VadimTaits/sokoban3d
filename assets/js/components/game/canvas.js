import React, { Component } from 'react';
import { Matrix, Vector } from 'sylvester';

import * as mapObjects from 'constants/map-objects';

import * as shaders from 'shaders';
import {
  initTexture, initWebGl,
  matrixEnsure4x4, matrixFlatten,
  makePerspective, matrixTranslation,
} from 'utils/webgl';

import styles from 'css/modules/game/canvas.css';

const cubeTextureCoordinates = [
  // Front
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0,
  // Back
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0,
  // Top
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0,
  // Bottom
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0,
  // Right
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0,
  // Left
  0.0,  0.0,
  1.0,  0.0,
  1.0,  1.0,
  0.0,  1.0
];

// This array defines each face as two triangles, using the
// indices into the vertex array to specify each triangle's
// position.

const cubeVertexIndices = [
  0,  1,  2,      0,  2,  3,    // front
  4,  5,  6,      4,  6,  7,    // back
  8,  9,  10,     8,  10, 11,   // top
  12, 13, 14,     12, 14, 15,   // bottom
  16, 17, 18,     16, 18, 19,   // right
  20, 21, 22,     20, 22, 23    // left
];

const vertexNormals = [
  // Front
   0.0,  0.0,  1.0,
   0.0,  0.0,  1.0,
   0.0,  0.0,  1.0,
   0.0,  0.0,  1.0,
  
  // Back
   0.0,  0.0, -1.0,
   0.0,  0.0, -1.0,
   0.0,  0.0, -1.0,
   0.0,  0.0, -1.0,
  
  // Top
   0.0,  1.0,  0.0,
   0.0,  1.0,  0.0,
   0.0,  1.0,  0.0,
   0.0,  1.0,  0.0,
  
  // Bottom
   0.0, -1.0,  0.0,
   0.0, -1.0,  0.0,
   0.0, -1.0,  0.0,
   0.0, -1.0,  0.0,
  
  // Right
   1.0,  0.0,  0.0,
   1.0,  0.0,  0.0,
   1.0,  0.0,  0.0,
   1.0,  0.0,  0.0,
  
  // Left
  -1.0,  0.0,  0.0,
  -1.0,  0.0,  0.0,
  -1.0,  0.0,  0.0,
  -1.0,  0.0,  0.0
];

const generateVertices = (sideWidth) => {
  const halfWidth = sideWidth / 2;

  return [
    // Front face
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0, -1.0, -1.0,

    // Top face
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
     1.0,  1.0,  1.0,
     1.0,  1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0,  1.0,
    -1.0, -1.0,  1.0,

    // Right face
     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
     1.0, -1.0,  1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0
  ].map((value) => value * halfWidth);
};

const rotationSpeed = Math.PI / 4000;

class Canvas extends Component {
  constructor(props) {
    super(props);

    this._buffersMap = new Map();
    this._time = Date.now();
  }

  initShaders() {
    const gl = this._gl;

    const shaderProgram = gl.createProgram();

    const vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader, shaders.vertex);
    gl.compileShader(vertexShader);

    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {  
      throw new Error("An error occurred compiling the shaders: " + gl.getShaderInfoLog(vertexShader));  
    }

    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShader, shaders.fragment);
    gl.compileShader(fragmentShader);

    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {  
      throw new Error("An error occurred compiling the shaders: " + gl.getShaderInfoLog(fragmentShader));  
    }

    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert('Unable to initialize the shader program.');
    }

    gl.useProgram(shaderProgram);

    const vertexPositionAttribute = gl.getAttribLocation(shaderProgram, 'aVertexPosition');
    gl.enableVertexAttribArray(vertexPositionAttribute);

    const textureCoordAttribute = gl.getAttribLocation(shaderProgram, 'aTextureCoord');
    gl.enableVertexAttribArray(textureCoordAttribute);

    const vertexNormalAttribute = gl.getAttribLocation(shaderProgram, 'aVertexNormal');
    gl.enableVertexAttribArray(vertexNormalAttribute);

    this._shaderProgram = shaderProgram;

    this._vertexPositionAttribute = vertexPositionAttribute;
    this._textureCoordAttribute = textureCoordAttribute;
    this._vertexNormalAttribute = vertexNormalAttribute;
  }

  initBuffersForOneCell(cell) {
    if (cell === mapObjects.EMPTY) {
      return;
    }

    const currentBuffers = this._buffersMap.get(cell) || [];

    let sideWidth;
    switch (cell) {
      case mapObjects.WALL:
        sideWidth = 2;
        break;

      case mapObjects.BOX:
        sideWidth = 1;
        break;

      case mapObjects.END_POINT:
        sideWidth = 0.5;
        break;

      case mapObjects.BOX_ON_END_POINT:
        sideWidth = 1;
        break;

      default:
        throw new Error(`Invalid cell type "${ cell }"`);
    };

    const gl = this._gl;

    const cubeVerticesTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesTextureCoordBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoordinates),
                  gl.STATIC_DRAW);

    // Create a buffer for the cube's vertices.

    const cubeVerticesBuffer = gl.createBuffer();

    // Select the cubeVerticesBuffer as the one to apply vertex
    // operations to from here out.

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesBuffer);

    // Now create an array of vertices for the cube.

    const vertices = generateVertices(sideWidth);

    // Now pass the list of vertices into WebGL to build the shape. We
    // do this by creating a Float32Array from the JavaScript array,
    // then use it to fill the current vertex buffer.

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

    // Build the element array buffer; this specifies the indices
    // into the vertex array for each face's vertices.

    const cubeVerticesIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVerticesIndexBuffer);

    // Now send the element array to GL

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);

    const cubeVerticesNormalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesNormalBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);

    currentBuffers.push({
      verticesTextureCoordBuffer: cubeVerticesTextureCoordBuffer,
      verticesBuffer: cubeVerticesBuffer,
      verticesIndexBuffer: cubeVerticesIndexBuffer,
      verticesNormalBuffer: cubeVerticesNormalBuffer,
    });

    this._buffersMap.set(cell, currentBuffers);
  }

  initBuffers() {
    this.props.map.forEach((row) => {
      row.forEach((cell) => {
        if ([
          mapObjects.WALL,
          mapObjects.BOX,
          mapObjects.END_POINT,
        ].includes(cell)) {
          this.initBuffersForOneCell(cell);
        }

        if (cell === mapObjects.BOX_ON_END_POINT) {
          this.initBuffersForOneCell(mapObjects.BOX);
          this.initBuffersForOneCell(mapObjects.END_POINT);
        }
      });
    });
  }

  initTextures() {
    return Promise.all([
      initTexture(this._gl, '/static/textures/box.png'),
      initTexture(this._gl, '/static/textures/end_point.png'),
      initTexture(this._gl, '/static/textures/wall.png'),
    ])
      .then(([box, endPoint, wall]) => {
        this._textures = {
          box,
          endPoint,
          wall,
        };
      });
  }

  drawScene() {
    const gl = this._gl;

    // Clear the canvas before we start drawing on it.

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Establish the perspective with which we want to view the
    // scene. Our field of view is 45 degrees, with a width/height
    // ratio of 640:480, and we only want to see objects between 0.1 units
    // and 100 units away from the camera.

    const perspectiveMatrix = makePerspective(45, 640.0/480.0, 0.1, 100.0);

    const usedBuffersCountMap = new Map();
    this.props.map.forEach((row, rowIndex) => {
      row.forEach((cell, cellIndex) => {
        if ([
          mapObjects.WALL,
          mapObjects.BOX,
          mapObjects.END_POINT,
        ].includes(cell)) {
          const bufferIndex = usedBuffersCountMap.get(cell) || 0;
          this.drawCell(perspectiveMatrix, cell, this._buffersMap.get(cell)[bufferIndex], cellIndex, rowIndex);
          usedBuffersCountMap.set(cell, bufferIndex + 1);
        }

        if (cell === mapObjects.BOX_ON_END_POINT) {
          [
            mapObjects.BOX,
            mapObjects.END_POINT,
          ].forEach((cell) => {
            const bufferIndex = usedBuffersCountMap.get(cell) || 0;
            this.drawCell(perspectiveMatrix, cell, this._buffersMap.get(cell)[bufferIndex], cellIndex, rowIndex);
            usedBuffersCountMap.set(cell, bufferIndex + 1);
          });
        }
      });
    });
  }

  drawCell(perspectiveMatrix, cell, buffers, cellIndex, rowIndex) {
    const gl = this._gl;
    const shaderProgram = this._shaderProgram;

    const vertexPositionAttribute = this._vertexPositionAttribute;
    const textureCoordAttribute = this._textureCoordAttribute;
    const vertexNormalAttribute = this._vertexNormalAttribute;

    const {
      verticesTextureCoordBuffer,
      verticesBuffer,
      verticesIndexBuffer,
      verticesNormalBuffer,
    } = buffers;

    let texture;
    switch (cell) {
      case mapObjects.WALL:
        texture = this._textures.wall;
        break;

      case mapObjects.BOX:
        texture = this._textures.box;
        break;

      case mapObjects.END_POINT:
        texture = this._textures.endPoint;
        break;

      default:
        throw new Error(`Invalid cell type "${ cell }"`);
    };

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.
    // Now move the drawing position a bit to where we want to start
    // drawing the cube.

    const {
      position: playerPosition,
      angleVector: playerAngleVector,
    } = this.props.player;

    const boxPosition = [(cellIndex - playerPosition[0]), (rowIndex - playerPosition[1])];

    const rotateMatrix = Matrix.create([
      [-playerAngleVector[1], -playerAngleVector[0]],
      [playerAngleVector[0], -playerAngleVector[1]],
    ]);

    const rotatedBoxPosition = Matrix.create([boxPosition])
      .multiply(rotateMatrix)
      .row(1).elements;

    if (rotatedBoxPosition[1] >= 0) {
      return;
    }

    const _mvMatrix = this.mvTranslate(this.loadIdentity(),
      [rotatedBoxPosition[0] * 2, 0.0, rotatedBoxPosition[1] * 2]);

    let mvMatrix;
    if (cell === mapObjects.BOX) {
      mvMatrix = _mvMatrix.multiply(
        matrixEnsure4x4(
          Matrix.Rotation(
            (Date.now() - this._time) * rotationSpeed,
            Vector.create([0, 1, 0]))));
    } else {
      mvMatrix = _mvMatrix;
    }

    // Draw the cube by binding the array buffer to the cube's vertices
    // array, setting attributes, and pushing it to GL.

    gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);
    gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

    // Set the texture coordinates attribute for the vertices.

    gl.bindBuffer(gl.ARRAY_BUFFER, verticesTextureCoordBuffer);
    gl.vertexAttribPointer(textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

    // Bind the normals buffer to the shader attribute.

    gl.bindBuffer(gl.ARRAY_BUFFER, verticesNormalBuffer);
    gl.vertexAttribPointer(vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

    // Specify the texture to map onto the faces.

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(gl.getUniformLocation(shaderProgram, 'uSampler'), 0);

    // Draw the cube.

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, verticesIndexBuffer);
    this.setMatrixUniforms(perspectiveMatrix, mvMatrix);
    gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
  }

  loadIdentity() {
    return Matrix.I(4);
  }

  mvTranslate(mvMatrix, v) {
    return mvMatrix.x(matrixEnsure4x4(matrixTranslation(Vector.create([v[0], v[1], v[2]]))));
  }

  setMatrixUniforms(perspectiveMatrix, mvMatrix) {
    const gl = this._gl;
    const shaderProgram = this._shaderProgram;

    const pUniform = gl.getUniformLocation(shaderProgram, 'uPMatrix');
    gl.uniformMatrix4fv(pUniform, false, new Float32Array(matrixFlatten(perspectiveMatrix)));

    const mvUniform = gl.getUniformLocation(shaderProgram, 'uMVMatrix');
    gl.uniformMatrix4fv(mvUniform, false, new Float32Array(matrixFlatten(mvMatrix)));

    const normalMatrix = mvMatrix.inverse().transpose();
    const nUniform = gl.getUniformLocation(shaderProgram, 'uNormalMatrix');
    gl.uniformMatrix4fv(nUniform, false, new Float32Array(matrixFlatten(normalMatrix)));
  }

  componentDidMount() {
    const gl = initWebGl(this._canvas);

    this._gl = gl;

    if (gl) {
      gl.clearColor(0.0, 0.0, 0.0, 1.0);
      gl.enable(gl.DEPTH_TEST);
      gl.depthFunc(gl.LEQUAL);
      gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

      this.initShaders();
      this.initBuffers();
      this.initTextures()
        .then(() => {
          this.drawScene();

          this._interval = setInterval(() => this.drawScene(), 15);
        });
    }
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }

  render() {
    return (
      <canvas
        className={ styles.canvas }
        ref={ (el) => this._canvas = el }
      />
    );
  }
}

export default Canvas;
