import React, { PropTypes } from 'react';
import cx from 'classnames';

import * as mapObjects from 'constants/map-objects';
/*
  EMPTY
  WALL
  BOX
  END_POINT
  BOX_ON_END_POINT
*/

import styles from 'css/modules/game/preview.css';

const cellWidth = parseInt(styles.cellWitdth);

const drawCell = (cell) => {
  switch (cell) {
    case mapObjects.EMPTY:
      return (
        <div className={ styles.cellEmpty } />
      );

    case mapObjects.WALL:
      return (
        <div className={ styles.cellWall } />
      );

    case mapObjects.BOX:
      return (
        <div className={ styles.cellBox } />
      );

    case mapObjects.END_POINT:
      return (
        <div className={ styles.cellEndPoint } />
      );

    case mapObjects.BOX_ON_END_POINT:
      return (
        <div className={ styles.cellBoxOnEndPoint } />
      );

    default:
      throw new Error(`Invalid cell "${ cell }"`);
  }
};

const Preview = ({
  map,
  player,
}) => (
  <div className={ styles.preview }>
    <div className={ styles.rows }>
      {
        map.map((row, rowIndex) => (
          <div className={ styles.row } key={ rowIndex }>
            {
              row.map((cell, cellIndex) => (
                <div className={ styles.cell } key={ cellIndex }>
                  { drawCell(cell) }
                </div>
              ))
            }
          </div>
        ))
      }
    </div>

    <div
      className={ cx(styles.player, {
        [styles.player_top]: player.angleVector[1] === -1,
        [styles.player_bottom]: player.angleVector[1] === 1,
        [styles.player_left]: player.angleVector[0] === -1,
        [styles.player_right]: player.angleVector[0] === 1,
      }) }
      style={{
        left: `${ cellWidth * player.position[0] }px`,
        top: `${ cellWidth * player.position[1] }px`,
      }}
    >
      <i className='fa fa-arrow-circle-up' />
    </div>
  </div>
);

export default Preview;
