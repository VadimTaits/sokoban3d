import React from 'react';
import cx from 'classnames';

import * as playerActions from 'constants/player-actions';

import styles from 'css/modules/game/controls.css';

const Controls = ({
  playerRotate,
  playerStep,
  restartLevel,
}) => (
  <div className={ styles.controls }>
    <div
      onClick={ playerStep.bind(null, playerActions.STEP_FORWARD) }
      className={ cx(styles.control, styles.forward) }
    >
      <i className='fa fa-long-arrow-up' />
    </div>

    <div
      onClick={ playerStep.bind(null, playerActions.STEP_BACKWARD) }
      className={ cx(styles.control, styles.backward) }
    >
      <i className='fa fa-long-arrow-down' />
    </div>

    <div
      onClick={ playerStep.bind(null, playerActions.STEP_LEFT) }
      className={ cx(styles.control, styles.strafeLeft) }
    >
      <i className='fa fa-long-arrow-left' />
    </div>

    <div
      onClick={ playerStep.bind(null, playerActions.STEP_RIGHT) }
      className={ cx(styles.control, styles.strafeRight) }
    >
      <i className='fa fa-long-arrow-right' />
    </div>

    <div
      onClick={ playerRotate.bind(null, playerActions.ROTATE_LEFT) }
      className={ cx(styles.control, styles.rotateLeft) }
    >
      <i className='fa fa-rotate-left' />
    </div>

    <div
      onClick={ playerRotate.bind(null, playerActions.ROTATE_RIGHT) }
      className={ cx(styles.control, styles.rotateRight) }
    >
      <i className='fa fa-rotate-right' />
    </div>

    <div
      onClick={ restartLevel }
      className={ cx(styles.control, styles.restart) }
    >
      Restart
    </div>
  </div>
);

export default Controls;
