import React from 'react';

import Canvas from './canvas';
import Controls from './controls';
import Preview from './preview';

import styles from 'css/modules/game/game.css';

const Game = ({
  levelFinished,
  levelLoaded,
  player,
  map,
  gameFinished,

  playerRotate,
  playerStep,
  loadNextLevel,
  restartLevel,
}) => {
  if (gameFinished) {
    return (
      <div className={ styles.gameFinished }>
        Congratulations!!! You are sokobaner!!!
      </div>
    );
  }

  if (!levelLoaded) {
    return null;
  }

  return (
    <div className={ styles.game }>
      <div className={ styles.canvas }>
        <Canvas
          map={ map }
          player={ player }
        />
      </div>

      <div className={ styles.previewAndControls }>
        <div className={ styles.preview }>
          <Preview
            map={ map }
            player={ player }
          />
        </div>

        <div className={ styles.controls }>
          <Controls
            playerRotate={ playerRotate }
            playerStep={ playerStep }
            restartLevel={ restartLevel }
          />
        </div>
      </div>

      {
        levelFinished && !gameFinished && (
          <div className={ styles.levelFinished }>
            <span
              className={ styles.loadNextButton }
              onClick={ loadNextLevel }
            >
              Go to next level
            </span>
          </div>
        )
      }
    </div>
  );
};

export default Game;
