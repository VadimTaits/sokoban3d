import {
  LOAD_LEVEL,
  LOAD_LEVEL_SUCCESS,
  LOAD_LEVEL_ERROR,

  RESTART_LEVEL,

  PLAYER_ROTATE,
  PLAYER_STEP,
} from 'actions/game';

const initialState = {
  currentLevel: 0,
  levelLoaded: false,
  levelInitialState: null,
  player: {
    position: [6, 2],
    angleVector: [1, 0],
  },
  map: [],
  boxesCount: 0,
  completedBoxesCount: 0,
  levelFinished: false,
  gameFinished: false,
};

export default function game(state = initialState, {
  type,
  payload,
}) {
  switch (type) {
    case LOAD_LEVEL:
      return {
        ...state,
        levelInitialState: null,
        levelLoaded: false,
        gameFinished: false,
      };

    case LOAD_LEVEL_SUCCESS:
      if (!payload.found) {
        return {
          ...state,
          gameFinished: true,
        };
      }

      return {
        ...state,
        levelLoaded: true,
        levelInitialState: {
          player: {
            position: payload.playerStartPosition,
            angleVector: [1, 0],
          },
          map: payload.map,
          boxesCount: payload.boxesCount,
          completedBoxesCount: payload.completedBoxesCount,
        },
        player: {
          position: payload.playerStartPosition,
          angleVector: [1, 0],
        },
        map: payload.map,
        boxesCount: payload.boxesCount,
        completedBoxesCount: payload.completedBoxesCount,
        currentLevel: payload.isNext ?
          state.currentLevel + 1 :
          state.currentLevel,
        levelFinished: false,
      };

    case PLAYER_ROTATE:
      return {
        ...state,
        player: {
          ...state.player,
          angleVector: payload.angleVector,
        },
      };

    case LOAD_LEVEL_SUCCESS:
      return 

    case PLAYER_STEP:
      return {
        ...state,
        player: {
          ...state.player,
          position: payload.positionTo,
        },
        map: payload.mapTranslations ?
          state.map.map((row, rowIndex) => {
            const rowTranslations = payload.mapTranslations
              .filter(({ position }) => position[1] === rowIndex);

            if (rowTranslations.length === 0) {
              return row;
            }

            return row.map((cell, cellIndex) => {
              const cellTranslation = rowTranslations
                .find(({ position }) => position[0] === cellIndex);

              if (!cellTranslation) {
                return cell;
              }

              return cellTranslation.object;
            });
          }) : state.map,

        completedBoxesCount: typeof payload.completedBoxesCount === 'undefined' ?
          state.completedBoxesCount :
          payload.completedBoxesCount,

        levelFinished: typeof payload.levelFinished === 'undefined' ?
          state.levelFinished :
          payload.levelFinished,
      };

    case RESTART_LEVEL:
      return {
        ...state,
        ...state.levelInitialState,
      };

    default:
      return state;
  }
};
