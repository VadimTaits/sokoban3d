import { Matrix } from 'sylvester';

import {
  getLevel as apiGetLevel,
} from 'api';

import * as playerActions from 'constants/player-actions';
import * as mapObjects from 'constants/map-objects';
/*
  EMPTY
  WALL
  BOX
  END_POINT
*/

import parseLevel from 'utils/parse-level';

export const PLAYER_ROTATE = 'game/ROTATE_PLAYER';
export const PLAYER_STEP = 'game/PLAYER_STEP';

export const LOAD_LEVEL = 'game/LOAD_LEVEL';
export const LOAD_LEVEL_SUCCESS = 'game/LOAD_LEVEL_SUCCESS';
export const LOAD_LEVEL_ERROR = 'game/LOAD_LEVEL_ERROR';

export const RESTART_LEVEL = 'game/RESTART_LEVEL';

const rotateForwardMatrix = Matrix.create([
  [1, 0],
  [0, 1],
]);

const rotateBackwardMatrix = Matrix.create([
  [-1, 0],
  [0, -1],
]);

const rotateLeftMatrix = Matrix.create([
  [0, -1],
  [1, 0],
]);

const rotateRightMatrix = Matrix.create([
  [0, 1],
  [-1, 0],
]);

function rotatePlayerAngleVector(currentAngleVector, rotateDirection) {
  const currentMatrix = Matrix.create([currentAngleVector]);

  let resultVector;
  switch (rotateDirection) {
    case playerActions.ROTATE_LEFT:
      resultVector = currentMatrix.multiply(rotateLeftMatrix);
      break;

    case playerActions.ROTATE_RIGHT:
      resultVector = currentMatrix.multiply(rotateRightMatrix);
      break;

    default:
      throw new Error(`Invalid rotation direction "${ direction }"`);
  }

  return Array.prototype.slice.call(resultVector.row(1).elements);
};

function getPlayerStepDirectionVector(currentAngleVector, stepDirection) {
  const currentMatrix = Matrix.create([currentAngleVector]);

  let resultVector;
  switch (stepDirection) {
    case playerActions.STEP_FORWARD:
      resultVector = currentMatrix.multiply(rotateForwardMatrix);
      break;

    case playerActions.STEP_BACKWARD:
      resultVector = currentMatrix.multiply(rotateBackwardMatrix);
      break;

    case playerActions.STEP_LEFT:
      resultVector = currentMatrix.multiply(rotateLeftMatrix);
      break;

    case playerActions.STEP_RIGHT:
      resultVector = currentMatrix.multiply(rotateRightMatrix);
      break;

    default:
      throw new Error(`Invalid step direction "${ direction }"`);
  }

  return Array.prototype.slice.call(resultVector.row(1).elements);
};

export function playerRotate(rotateDirection) {
  return (dispatch, getState) => {
    const {
      game: {
        player,
        levelFinished,
        gameFinished,
      },
    } = getState();

    if (levelFinished || gameFinished) {
      return;
    }

    dispatch({
      type: PLAYER_ROTATE,
      payload: {
        angleVector: rotatePlayerAngleVector(
          player.angleVector, rotateDirection),
      },
    });
  };
};

export function playerStep(stepDirection) {
  return (dispatch, getState) => {
    const {
      game: {
        map,
        player,
        levelFinished,
        gameFinished,
        completedBoxesCount,
        boxesCount,
      },
    } = getState();

    if (levelFinished || gameFinished) {
      return;
    }

    const stepDirectionVector = getPlayerStepDirectionVector(
      player.angleVector, stepDirection);

    const positionTo = player.position
      .map((value, index) => value + stepDirectionVector[index]);

    const mapObject = map[positionTo[1]][positionTo[0]];

    switch (mapObject) {
      case mapObjects.EMPTY:
      case mapObjects.END_POINT:
        dispatch({
          type: PLAYER_STEP,
          payload: {
            positionTo,
          },
        });
        return;

      case mapObjects.WALL:
        return;

      case mapObjects.BOX:
      case mapObjects.BOX_ON_END_POINT:
        const boxPositionTo = positionTo
          .map((value, index) => value + stepDirectionVector[index]);

        const nextMapObject = map[boxPositionTo[1]][boxPositionTo[0]];

        const completedBoxesDelta = (nextMapObject === mapObjects.END_POINT ? 1 : 0) -
          (mapObject === mapObjects.BOX_ON_END_POINT ? 1 : 0);

        const newCompletedBoxesCount = completedBoxesCount + completedBoxesDelta;

        switch (nextMapObject) {
          case mapObjects.EMPTY:
          case mapObjects.END_POINT:
            dispatch({
              type: PLAYER_STEP,
              payload: {
                positionTo,
                mapTranslations: [{
                  position: positionTo,
                  object: mapObject === mapObjects.BOX ?
                    mapObjects.EMPTY : mapObjects.END_POINT,
                }, {
                  position: boxPositionTo,
                  object: nextMapObject === mapObjects.EMPTY ?
                    mapObjects.BOX : mapObjects.BOX_ON_END_POINT,
                }],
                completedBoxesCount: newCompletedBoxesCount,
                levelFinished: newCompletedBoxesCount === boxesCount,
              },
            });
            return;

          case mapObjects.WALL:
          case mapObjects.BOX:
          case mapObjects.BOX_ON_END_POINT:
            return;

          default:
            throw new Error(`Invalid map object "${ mapObject }"`);
        }

        return;

      default:
        throw new Error(`Invalid map object "${ mapObject }"`);
    }
  };
};

export function loadLevel(isNext) {
  return (dispatch, getState) => {
    const {
      game: {
        currentLevel,
      },
    } = getState();

    dispatch({
      type: LOAD_LEVEL,
    });

    apiGetLevel(isNext ? currentLevel : null)
      .then(({ found, level }) => {
        if (found) {
          const {
            playerStartPosition,
            map,
            boxesCount,
            completedBoxesCount,
          } = parseLevel(level);

          dispatch({
            type: LOAD_LEVEL_SUCCESS,
            payload: {
              found,
              playerStartPosition,
              map,
              boxesCount,
              completedBoxesCount,
              isNext,
            },
          });

          return;
        }

        dispatch({
          type: LOAD_LEVEL_SUCCESS,
          payload: {
            found,
            isNext,
          },
        });
      }, () => {
        dispatch({
          type: LOAD_LEVEL_ERROR,
        });
      });
  };
};

export function restartLevel() {
  return {
    type: RESTART_LEVEL,
  };
};
