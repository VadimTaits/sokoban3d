import 'normalize.css';
import 'font-awesome/css/font-awesome.css';
import 'css/plain/styles.css';

import 'babel-polyfill';
import 'whatwg-fetch';

import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';

import Game from './containers/game';

import store from './store';

import {
  loadLevel,
  playerRotate,
  playerStep,
} from 'actions/game';

import * as playerActions from 'constants/player-actions';

store.dispatch(loadLevel(false));

render(
  <Provider store={ store }>
    <Game />
  </Provider>,
  document.getElementById('app'),
);

document.addEventListener('keydown', ({ keyCode }) => {
  switch (keyCode) {
    case 87:
    case 38:
      store.dispatch(playerStep(playerActions.STEP_FORWARD));
      break;

    case 83:
    case 40:
      store.dispatch(playerStep(playerActions.STEP_BACKWARD));
      break;

    case 65:
      store.dispatch(playerStep(playerActions.STEP_LEFT));
      break;

    case 68:
      store.dispatch(playerStep(playerActions.STEP_RIGHT));
      break;

    case 37:
      store.dispatch(playerRotate(playerActions.ROTATE_LEFT));
      break;

    case 39:
      store.dispatch(playerRotate(playerActions.ROTATE_RIGHT));
      break;

    default:
      break;
  }
});
