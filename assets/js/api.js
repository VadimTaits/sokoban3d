const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response); 
  }

  if (response.status === 401) {
    window.location.href = '/';
    return new Promise(() => {});
  }

  return new Promise((resolve, reject) => {
    response.json()
      .then((json) => {
        if (json.message) {
          errorLog(json.message);
        }

        reject(json);
      });
  });
};

const toJson = (response) => response.json();
const defaultOptions = {
  credentials: 'same-origin',
  headers: {
    'Accept': 'application/json',
  },
};

function makeFetch(url, options = {}) {
  return fetch(url, {
    ...options,
    credentials: 'same-origin',
  })
    .then(checkStatus);
};

export function getLevel(prevLevel) {
  const url = typeof prevLevel === 'number' ?
    `/api/levels/?prev=${ prevLevel }` :
    '/api/levels/';

  return makeFetch(url)
    .then(toJson);
};
