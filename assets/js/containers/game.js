import { connect } from 'react-redux';

import Game from 'components/game';

import {
  playerRotate,
  playerStep,
  loadLevel,
  restartLevel,
} from 'actions/game';

const mapStateToProps = ({
  game,
}) => game;

const mapDispatchToProps = {
  playerRotate,
  playerStep,
  loadNextLevel: loadLevel.bind(null, true),
  restartLevel,
};

export default connect(mapStateToProps, mapDispatchToProps)(Game);
