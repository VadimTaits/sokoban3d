import * as mapObjects from 'constants/map-objects';

export default function parseLevel(levelStr) {
  let playerStartPosition;
  let map = [];
  let boxesCount = 0;
  let completedBoxesCount = 0;

  levelStr
    .split('\n')
    .filter((levelStrRow) => !!levelStrRow.trim())
    .forEach((levelStrRow, rowIndex) => {
      let mapRow = [];

      Array.prototype.slice.call(levelStrRow)
        .filter((cellChar) => !!cellChar.trim())
        .forEach((cellChar, colIndex) => {
          switch (cellChar) {
            case 'e':
              mapRow.push(mapObjects.EMPTY);
              return;

            case 'w':
              mapRow.push(mapObjects.WALL);
              return;

            case 'p':
              mapRow.push(mapObjects.END_POINT);
              return;

            case 'b':
              ++boxesCount;
              mapRow.push(mapObjects.BOX);
              return;

            case 'c':
              ++boxesCount;
              ++completedBoxesCount;
              mapRow.push(mapObjects.BOX_ON_END_POINT);
              return;

            case 's':
              mapRow.push(mapObjects.EMPTY);
              playerStartPosition = [colIndex, rowIndex];
              return;

            default:
              throw new Error(`Invalid cell "${ cellChar }"`);
          }
        });

      map.push(mapRow);
    });

  return {
    playerStartPosition,
    map,
    boxesCount,
    completedBoxesCount,
  };
};
