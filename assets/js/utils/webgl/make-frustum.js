import { Matrix } from 'sylvester';

export default function makeFrustum(left, right, bottom, top, znear, zfar) {
  const X = 2*znear/(right-left);
  const Y = 2*znear/(top-bottom);
  const A = (right+left)/(right-left);
  const B = (top+bottom)/(top-bottom);
  const C = -(zfar+znear)/(zfar-znear);
  const D = -2*zfar*znear/(zfar-znear);

  return Matrix.create([
    [X, 0, A, 0],
    [0, Y, B, 0],
    [0, 0, C, D],
    [0, 0, -1, 0],
  ]);
};
