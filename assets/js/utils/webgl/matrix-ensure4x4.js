export default function matrixEnsure4x4(matrix) {
    if (matrix.elements.length == 4 &&
        matrix.elements[0].length == 4)
        return matrix;

    if (matrix.elements.length > 4 ||
        matrix.elements[0].length > 4)
        return null;

    for (var i = 0; i < matrix.elements.length; i++) {
        for (var j = matrix.elements[i].length; j < 4; j++) {
            if (i == j)
                matrix.elements[i].push(1);
            else
                matrix.elements[i].push(0);
        }
    }

    for (var i = matrix.elements.length; i < 4; i++) {
        if (i == 0)
            matrix.elements.push([1, 0, 0, 0]);
        else if (i == 1)
            matrix.elements.push([0, 1, 0, 0]);
        else if (i == 2)
            matrix.elements.push([0, 0, 1, 0]);
        else if (i == 3)
            matrix.elements.push([0, 0, 0, 1]);
    }

    return matrix;
};
