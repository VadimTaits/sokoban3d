export default function matrixFlatten(matrix) {
  if (matrix.elements.length == 0) {
    return [];
  }

  let result = [];

  for (let j = 0; j < matrix.elements[0].length; j++) {
    for (let i = 0; i < matrix.elements.length; i++) {
      result.push(matrix.elements[i][j]);
    }
  }

  return result;
};
