import makeFrustum from './make-frustum';

export default function makePerspective(fovy, aspect, znear, zfar) {
  const ymax = znear * Math.tan(fovy * Math.PI / 360.0);
  const ymin = -ymax;
  const xmin = ymin * aspect;
  const xmax = ymax * aspect;

  return makeFrustum(xmin, xmax, ymin, ymax, znear, zfar);
};
