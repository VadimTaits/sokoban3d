'use strict';

const settings = require('./settings');

require('babel-register')({});

const server = require('./server').default;
const PORT = settings.port || 3000;

server.listen(PORT, function () {
  console.log('Server listening on', PORT);
});
